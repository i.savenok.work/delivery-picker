# Delivery slot picker

## Setup and launch

1. create volume for MySQL data:
>docker volume create --name=picker_mysql_data
2. Start docker-compose:
>docker-compose up

**Delivery slot picker** will be available at **http://localhost:8080/**

**Admin pages** will be available at **http://localhost:8080/admin/** <br>
**Admin user credentials**:
> username: admin <br>
> password: 1234

**API** will be available at **http://localhost:8080/api/v1/**

## Usage

### 1. Delivery slots templates configuration

In the default configuration, there are 3 slots for each day
with individual capacity and time intervals.
You can change the configuration for the next slot generation
session at **Base delivery slots** admin pages (http://localhost:8080/admin/delivery_slot/basedeliveryslot/).

![img](docs/base-delivery-slot-admin.png)

**Base delivery slot** is a "template" for **delivery slot** generation for each day. <br>

**Start time** and **End time** define a time interval in one day of the delivery slot. <br>
**Max capacity** defines the number of deliveries available for the slot.

### 2. Delivery slots generation

Delivery slots generation can be started at **Delivery slots** admin pages
(http://127.0.0.1:8080/admin/delivery_slot/deliveryslot/):

![img](docs/slots-generation.gif)

Each generation process call will generate slots based on **Base delivery slots**
starting from the last existing slot.

Manual slots generation launch:

>python manage.py generate_delivery_slots {number of weeks (by default - 4)}

### 3. Delivery items edition

Delivery items can be edited using **Delivery items** admin pages
(http://127.0.0.1:8080/admin/delivery_item/deliveryitem/).

### 4. Creating deliveries

1. Select at least 1 item:
![img](docs/items-selection.gif)
2. Choose free delivery slot:
![img](docs/slots-selection.gif)
3. Submit delivery:
![img](docs/delivery-submit.gif)

## API schema

API schema located in project root in **schema.graphql**
