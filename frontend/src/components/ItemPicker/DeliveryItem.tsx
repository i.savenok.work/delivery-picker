import {DeliveryItemType} from "../../common/apiClient";
import {ItemComment, ItemWrapper} from "./styled";
import React from "react";


export function DeliveryItem(props: {item: DeliveryItemType, selected: boolean, onClick: {(): void}|undefined}) {
    return <ItemWrapper
        key={props.item.id}
        selected={props.selected}
        notSelectable={props.onClick === undefined}
        onClick={props.onClick}
    >
        <div>{props.item.name}</div>
        {props.item.notAvailableOnWednesday ? <ItemComment>Not available on Wednesdays</ItemComment> : undefined}
    </ItemWrapper>
}
