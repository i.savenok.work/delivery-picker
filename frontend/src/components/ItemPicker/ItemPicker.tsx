import {ApiClient, DeliveryItemType} from "../../common/apiClient";
import React, {useEffect, useState} from "react";
import {ItemPickerSubmitButton, ItemPickerTip, ItemPickerWrapper} from "./styled";
import {DeliveryItem} from "./DeliveryItem";
import {Toaster} from "../../common/toast";


export type ItemsSelectionDetailsType = {items: DeliveryItemType[], wednesdaysNotAvailable: boolean}


export function ItemPicker(props: {
    selectedItems: DeliveryItemType[],
    onSubmit: {(data: ItemsSelectionDetailsType): void},
}) {
    const [allItems, setAllItems] = useState<{[key:string]:DeliveryItemType}>({});
    const [selectedItems, setSelectedItems] = useState<DeliveryItemType[]>(props.selectedItems);

    function loadDeliveryItems() {
        ApiClient.getDeliveryItems(
            (items) => {
                setAllItems(Object.fromEntries(items.map(item => [item.id, item])))
            },
            null
        )
    }

    useEffect(() => {
        loadDeliveryItems();
    }, [])

    return <>
        <ItemPickerWrapper>
            <ItemPickerSubmitButton onClick={() => {
                if (selectedItems.length === 0) { Toaster.info('Select at least one item'); return }
                const wednesdaysNotAvailable = selectedItems.find(i => allItems[i.id]?.notAvailableOnWednesday) !== undefined
                props.onSubmit({items: selectedItems, wednesdaysNotAvailable: wednesdaysNotAvailable})
            }}>
                Submit
            </ItemPickerSubmitButton>

            {Object.values(allItems).map((item) => {
                const isItemSelected = selectedItems.map(i => i.id).includes(item.id)
                return <DeliveryItem
                    item={item}
                    selected={isItemSelected}
                    onClick={() => {
                        isItemSelected ?
                            setSelectedItems(selectedItems.filter(i => i.id !== item.id))
                            :
                            setSelectedItems([...selectedItems, item])
                    }}
                />
            })}


        </ItemPickerWrapper>
        <ItemPickerTip>Select delivery items</ItemPickerTip>
    </>
}
