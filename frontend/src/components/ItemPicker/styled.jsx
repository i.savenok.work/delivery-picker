import styled from "styled-components";


export const ItemPickerWrapper = styled.div`
    position: fixed;
    width: 15%;
    height: 100%;

    top:0;
    left: 0;

    background-color: #ffffff;
`


export const ItemWrapper = styled.div`
  margin: 5px;
  padding: 3px;
  min-height: 40px;

  background-color: ${props => props.selected ? "#b8d6b2" : "#d6d6d6"};
  cursor: ${props => props.notSelectable ? "default" : "pointer"};
`


export const ItemComment = styled.div`
  font-size: smaller;
`


export const ItemPickerSubmitButton = styled.div`
  margin: 5px 5px 5px 5px;
  height: 40px;
  padding: 3px;
  background-color: #82a67b;
  cursor: pointer;

    &:hover {
        background-color: #6a8764;
        color: white;
    }
`

export const ItemPickerTip = styled.div`
  margin-left: 15%;
  padding-left: 10px;
  padding-top: 10px;
  position: fixed;
`
