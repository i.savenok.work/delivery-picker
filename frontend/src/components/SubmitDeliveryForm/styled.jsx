import styled from "styled-components";


export const SubmitDeliveryFormModal = styled.div`
  display: block;
  position: fixed;
  z-index: 100;
  left: 15%;
  top: 0;
  width: 100%;
  height: 100%;
  overflow: auto;
  background-color: rgb(0,0,0);
  background-color: rgba(0,0,0,0.2);
`
export const SubmitDeliveryFormWrapper = styled.div`
    margin-top: 10px;
    width: 600px;
    display: flex;
    flex-direction: column;
    padding: 5px;
    z-index: 101;
`

export const DeliveryTotalInfo = styled.div`
    background-color: white;
    min-height: 30px;
    padding: 5px;
`
export const DeliveryButtonsWrapper = styled.div`
    margin-top: 5px;
    display: flex;
    flex-direction: row;
    height: 30px;
    justify-content: space-between;
`
export const DeliverySubmitButton = styled.div`
    margin-right: 5px;
    display: flex;
    flex-direction: row;
    height: 30px;
    flex-basis: 400px;
    justify-content: center;
    align-items: center;
    background-color: #82a67b;
    cursor: pointer;

    &:hover {
        background-color: #6a8764;
        color: white;
    }
`
export const DeliveryCancelButton = styled.div`
    display: flex;
    flex-direction: row;
    height: 30px;
    flex-basis: 200px;
    justify-content: center;
    align-items: center;
    background-color: #db9291;
    cursor: pointer;

    &:hover {
        background-color: #8f4e4d;
        color: white;
    }
`
