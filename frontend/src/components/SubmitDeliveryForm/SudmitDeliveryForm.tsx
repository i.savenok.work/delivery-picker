import React, {useState} from "react";
import {ApiClient, DeliverySlotType} from "../../common/apiClient";
import {
    DeliveryButtonsWrapper,
    DeliveryCancelButton,
    DeliverySubmitButton,
    SubmitDeliveryFormModal,
    SubmitDeliveryFormWrapper
} from "./styled";
import {Toaster} from "../../common/toast";


export function SubmitDeliveryForm(props: {
    selectedItemsIds: string[],
    selectedSlot: DeliverySlotType,
    onSuccess: {(isSlotCapacityOver: boolean):void},
    onDecline: {(slotsOutdated: boolean, itemsOutdated: boolean): void},
}) {
    const [isLoading, setLoading] = useState<boolean>(false);

    function submit() {
        if (isLoading) { return }
        setLoading(true)
        ApiClient.createDelivery(
            props.selectedSlot.id,
            props.selectedItemsIds,
            (data => { Toaster.success('Delivery scheduled'); props.onSuccess(data.slot.outOfCapacity) }),
            (error => {
                const isSlotOutdated = error === ApiClient.ERR_SLOT_NOT_AVAILABLE;
                const isItemsOutdated = error === ApiClient.ERR_ITEMS_NOT_EXIST;
                if (isSlotOutdated) { Toaster.warn('Selected slot was changed') }
                else if (isItemsOutdated) { Toaster.warn('Selected items were changed') }
                else { Toaster.error('Check your network connection') }
                props.onDecline(isSlotOutdated, isItemsOutdated)
            }),
        )
    }

    return <>
        <SubmitDeliveryFormModal/>
        <SubmitDeliveryFormWrapper>
            {
                !isLoading ?
                    <DeliveryButtonsWrapper>
                        <DeliverySubmitButton onClick={submit}>Submit</DeliverySubmitButton>
                        <DeliveryCancelButton onClick={() => {props.onDecline(false, false)}}>Cancel</DeliveryCancelButton>
                    </DeliveryButtonsWrapper>
                    :
                    undefined
            }
        </SubmitDeliveryFormWrapper>
    </>
}
