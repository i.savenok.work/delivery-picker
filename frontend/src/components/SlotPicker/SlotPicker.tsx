import React, {useEffect, useState} from "react";
import { addDays } from "../../common/datetime";
import {ApiClient, DeliverySlotType} from "../../common/apiClient";
import {ItemsSelectionDetailsType} from "../ItemPicker/ItemPicker";
import {isLastIndex, isLastIsNull} from "../../common/array";
import {SubmitDeliveryForm} from "../SubmitDeliveryForm/SudmitDeliveryForm";
import {ItemPickerSubmitButton, ItemPickerWrapper} from "../ItemPicker/styled";
import {DeliveryItem} from "../ItemPicker/DeliveryItem";
import {
    SlotPickerNavigationButton,
    SlotPickerSelectorContainer,
    SlotPickerTitleContainer,
    SlotPickerWrapper,
    SlotsPackContainer, SlotsPackStubContainer
} from "./styled";
import {Toaster} from "../../common/toast";
import {SlotPackType, transformSlotsListToSlotsPack} from "./SlotPack";
import {SlotPickerDay} from "./SlotPickerDay";


export function SlotPicker(props: {itemsSelectionDetails: ItemsSelectionDetailsType, changeItemsSelection: {(): void}}) {
    const [selectedSlotsPackIndex, setSelectedSlotsPackIndex] = useState<number>(0)
    const [slotsPacks, setSlotsPacks] = useState<(SlotPackType|null)[]>([]);
    const [selectedSlot, setSelectedSlot] = useState<DeliverySlotType|null>(null);

    function loadNextSlotsPack() {
        let lastSlotDate: Date|undefined
        if (isLastIndex(slotsPacks, selectedSlotsPackIndex) && isLastIsNull(slotsPacks)) {
            lastSlotDate = slotsPacks.at(-2)?.at(-1)?.date
            if (lastSlotDate) { lastSlotDate = addDays(lastSlotDate, 1) } else if (selectedSlotsPackIndex !== 0) { return }
        } else {
            lastSlotDate = slotsPacks.at(-1)?.at(-1)?.date
        }

        ApiClient.getDeliverySlots(
            lastSlotDate ? addDays(lastSlotDate, 1) : new Date(),
            4,
            true,
            props.itemsSelectionDetails.wednesdaysNotAvailable,
            (slotsData) => {
                if (slotsData.length === 0) {
                    if (!isLastIsNull(slotsPacks)) { setSlotsPacks([...slotsPacks, null]) }
                    Toaster.info('No data')
                    return
                }

                if (!isLastIsNull(slotsPacks)) {
                    setSlotsPacks([...slotsPacks, transformSlotsListToSlotsPack(slotsData)])
                } else {
                    setSlotsPacks([...slotsPacks.slice(0, -1), transformSlotsListToSlotsPack(slotsData)])
                }
            },
            null,
        )
    }

    function showNextPack() {
        if (selectedSlot) { return }
        if (isLastIndex(slotsPacks, selectedSlotsPackIndex) && isLastIsNull(slotsPacks)) {
            loadNextSlotsPack()
        } else {
            if (slotsPacks[selectedSlotsPackIndex + 1] === undefined) { loadNextSlotsPack() }
            setSelectedSlotsPackIndex(selectedSlotsPackIndex + 1)
        }
    }
    function showPreviousPack() {
        if (selectedSlot) { return }
        if (selectedSlotsPackIndex > 0) {setSelectedSlotsPackIndex(selectedSlotsPackIndex - 1)}
    }

    useEffect(() => {
        if (slotsPacks[selectedSlotsPackIndex] === undefined) {
            loadNextSlotsPack()
        }
    }, [slotsPacks])

    return <SlotPickerWrapper>
        <ItemPickerWrapper>
            <ItemPickerSubmitButton onClick={props.changeItemsSelection}>Change items selection</ItemPickerSubmitButton>
            {props.itemsSelectionDetails.items.map(item => {return <DeliveryItem item={item} selected={true} onClick={undefined}/>})}
        </ItemPickerWrapper>

        <SlotPickerTitleContainer>Select delivery slot:</SlotPickerTitleContainer>

        <SlotPickerSelectorContainer>
            <SlotPickerNavigationButton hide={selectedSlotsPackIndex < 1} onClick={showPreviousPack}>
                prev
            </SlotPickerNavigationButton>
            <SlotsPackContainer>
                {
                    slotsPacks[selectedSlotsPackIndex]
                    ?
                    slotsPacks[selectedSlotsPackIndex]!.map((deliveryDay, index) => {
                        return <SlotPickerDay
                            key={index}
                            date={deliveryDay.date}
                            slots={deliveryDay.slots}
                            wednesdaysNotAvailable={props.itemsSelectionDetails.wednesdaysNotAvailable}
                            selectedSlotId={selectedSlot?.id}
                            selectedSlotSetter={setSelectedSlot}
                        />
                    })
                    :
                    <SlotsPackStubContainer>No data</SlotsPackStubContainer>
                }
            </SlotsPackContainer>
            <SlotPickerNavigationButton onClick={showNextPack}>
                {isLastIndex(slotsPacks, selectedSlotsPackIndex) && isLastIsNull(slotsPacks) ? "reload" : "next"}
            </SlotPickerNavigationButton>
        </SlotPickerSelectorContainer>

        {
            selectedSlot
            ?
            <SubmitDeliveryForm
                selectedSlot={selectedSlot}
                selectedItemsIds={props.itemsSelectionDetails.items.map(i => i.id)}
                onSuccess={(isSlotCapacityOver) => {
                    if (isSlotCapacityOver) {
                        const selectedSlotsPack = slotsPacks[selectedSlotsPackIndex]
                        selectedSlotsPack?.forEach((day, dayIndex) => {
                            day.slots.forEach((slot, inDayIndex) => {
                                if (slot.id === selectedSlot.id) {

                                    const isFirstDayInPack = dayIndex === 0
                                    if (isFirstDayInPack) {
                                        const numOfAvailableSlots = day.slots.filter(s => !s.outOfCapacity && !s.notAvailable).length - 1
                                        if (numOfAvailableSlots < 1) {
                                            // reload current slotPack, remove packs after current
                                            const newSlotsPacks = [...slotsPacks.slice(0, selectedSlotsPackIndex)]
                                            setSlotsPacks(newSlotsPacks)
                                        }
                                    }
                                    const newSlotsPacks = [...slotsPacks]
                                    newSlotsPacks[selectedSlotsPackIndex]![dayIndex].slots[inDayIndex].outOfCapacity = true
                                    setSlotsPacks(newSlotsPacks)
                                }
                            })
                        })
                    }
                    setSelectedSlot(null)
                }}
                onDecline={(slotsOutdated, itemsOutdated) => {
                    if (itemsOutdated) {
                        props.changeItemsSelection()
                    } else if (slotsOutdated) {
                        const newSlotsPacks = [...slotsPacks.slice(0, selectedSlotsPackIndex)]
                        setSlotsPacks(newSlotsPacks)
                    }
                    setSelectedSlot(null)
                }}
            />
            :
            undefined
        }
    </SlotPickerWrapper>
}
