import styled from "styled-components";


export const SlotPickerWrapper = styled.div`
    position: fixed;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;

    top:0;
    left: 15%;
`

export const SlotPickerSelectorContainer = styled.div`
      display: flex;
      flex-direction: row;
`

export const SlotPickerTitleContainer = styled.div`
      margin: 10px 0 10px 0;
`
export const SlotPickerNavigationButton = styled.div`
      background-color: #ffffff;
      margin: 150px 25px 0 25px;
      width: 75px;
      height: 75px;
      border-radius: 50%;
      display: flex;
      justify-content: center;
      align-items: center;
      cursor: pointer;

      visibility: ${props => props.hide ? "hidden" : "visible"};
`
export const SlotsPackContainer = styled.div`
      display: flex;
      margin: 1px;
`
export const SlotsPackStubContainer = styled.div`
    width: 600px;
    display: flex;
    justify-content: center;
    align-items: center;
`

export const SlotPickerDayContainer = styled.div`
    width: 150px;
`
export const SlotPickerDayTitle = styled.div`
    padding: 10px;
    margin: 1px;
    cursor: pointer;
    background-color: #ffffff;


`
export const SlotPickerDayTitleWeekDay = styled.div`
   font-size: smaller;
`
export const SlotContainer = styled.div`
    min-height: 100px;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    margin: 1px;
    cursor: pointer;

    background-color: ${props => {
        if (props.unavailable) { return "#d6d6d6" }
        if (props.full) { return "#db9291" }
        if (props.selected) { return "#6a8764" }
        return "#b8d6b2"
    }};
    color: ${props => {
        if (props.selected) { return "white" }
        return "auto"
    }};

    &:hover {
        background-color: ${props => {
        if (!props.selected && !props.unavailable && !props.full) { return "#82a67b" }
        return "auto"
    }};
    }
`
