import {SlotContainer, SlotPickerDayContainer, SlotPickerDayTitle, SlotPickerDayTitleWeekDay} from "./styled";
import {getDateBeautyString, getWeekdayBeautyString} from "../../common/datetime";
import {Toaster} from "../../common/toast";
import React from "react";
import {DeliverySlotType} from "../../common/apiClient";


export function SlotPickerDay(props: {
    date: Date,
    slots: DeliverySlotType[],
    wednesdaysNotAvailable: boolean,
    selectedSlotId: string|undefined,
    selectedSlotSetter: {(slot: DeliverySlotType): void}
}) {
    return <SlotPickerDayContainer>
        <SlotPickerDayTitle>
            <div>{getDateBeautyString(props.date)}</div>
            <SlotPickerDayTitleWeekDay>{getWeekdayBeautyString(props.date)}</SlotPickerDayTitleWeekDay>
        </SlotPickerDayTitle>
        {props.slots.map((slot) => {
            const slotWeekDay = props.date.getDay()
            const slotIsOnWednesdayButWednesdaysUnavailable = (props.wednesdaysNotAvailable && slotWeekDay === 3)
            return <SlotContainer
                key={slot.id}
                onClick={() => {
                    if (props.selectedSlotId) { return }
                    else if (slot.notAvailable) { Toaster.info('Slot is first in the odd Friday of the week') }
                    else if (slotIsOnWednesdayButWednesdaysUnavailable) { Toaster.info('One of selected items is unavailable on Wednesdays') }
                    else if (slot.outOfCapacity) { Toaster.info('Slot is out of capacity') }
                    else { props.selectedSlotSetter(slot) }
                }}
                unavailable={slotIsOnWednesdayButWednesdaysUnavailable || slot.notAvailable}
                full={slot.outOfCapacity}
                selected={props.selectedSlotId === slot.id}
            >
                {slot.startTime.slice(0, 5)} - {slot.endTime.slice(0, 5)}
            </SlotContainer>
        })}
    </SlotPickerDayContainer>
}
