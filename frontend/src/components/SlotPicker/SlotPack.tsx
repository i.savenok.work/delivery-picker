import {DeliverySlotType} from "../../common/apiClient";
import {forcePush} from "../../common/object";
import {addDays, getDateISOString} from "../../common/datetime";


export type SlotPackType = {slots: DeliverySlotType[], date: Date}[]
export function transformSlotsListToSlotsPack(slots: DeliverySlotType[]): SlotPackType {
    const slotsByDateStrings: {[key:string]:DeliverySlotType[]} = {}
    slots.forEach((slot) => { forcePush(slotsByDateStrings, slot.date, slot) })

    const slotsPack: SlotPackType = []
    const firstDate = new Date(Object.keys(slotsByDateStrings)[0])
    for (let i = 0; i < 4; i++) {
        const lookupDate = addDays(firstDate, i)
        const lookupDateString = getDateISOString(lookupDate)
        slotsPack.push(
            lookupDateString in slotsByDateStrings ?
                {slots: slotsByDateStrings[lookupDateString], date: lookupDate}
                :
                {slots: [], date: lookupDate}
        )
    }
    return slotsPack
}
