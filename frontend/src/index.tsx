import React, {useState} from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import {ItemPicker, ItemsSelectionDetailsType} from "./components/ItemPicker/ItemPicker";
import {SlotPicker} from "./components/SlotPicker/SlotPicker";
import {ToastContainer} from "react-toastify";
import "react-toastify/dist/ReactToastify.css";


function App() {
    const [deliveryItemsAreSelected, setDeliveryItemsAreSelected] = useState<boolean>(false);
    const [itemsSelectionDetails, setItemsSelectionDetails] = useState<ItemsSelectionDetailsType>(
        {items: [], wednesdaysNotAvailable: false}
    );

    if (!deliveryItemsAreSelected) {
        return <ItemPicker
            selectedItems={itemsSelectionDetails.items}
            onSubmit={(details) => {
                setItemsSelectionDetails(details)
                setDeliveryItemsAreSelected(true)
            }}
        />
    } else {
        return <SlotPicker
            itemsSelectionDetails={itemsSelectionDetails}
            changeItemsSelection={() => setDeliveryItemsAreSelected(false)}
        />
    }
}


ReactDOM.render(
  <React.StrictMode>
      <ToastContainer position="top-right"/>
      <App />
  </React.StrictMode>,
  document.getElementById('root')
);
