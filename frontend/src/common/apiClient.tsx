import {Env} from "../settings";
import {getDateISOString} from "./datetime";


class Query {
    raw: string;
    onSuccess: any
    onError: ErrorCallbackType;

    constructor(template: string, variables: object | null = null) {
        this.raw = JSON.stringify({
            query: template,
            variables: variables ? variables : {},
        })
        this.onSuccess = null;
        this.onError = null
    }

    _onSuccess(data: any) {if (this.onSuccess) {this.onSuccess((data.data ? data.data : data))}}
    _onError(message: string) {
        console.log(`Api error ${this.raw}: ${message}`);
        if (this.onError) {this.onError(message)}
    }
    execute() {
        try {
            fetch(
                `${Env.API_HOST}:${Env.API_PORT}${Env.API_PREFIX}`, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                    "Accept": "application/json",
                },
                body: this.raw,
            }).then(
                (response) => {
                    if (!response.ok || response.status !== 200) {
                        response.json().then((data) => {this._onError(data)})
                    } else {
                        response.json().then((data) => {this._onSuccess(data)})
                    }
                },
                (error) => {this._onError(error)}
            )
        } catch (error) { // @ts-ignore
            this._onError(error)}
    }
}


export class ApiClient {

    static getDeliveryItems(onSuccess: {(data: Array<DeliveryItemType>):void}, onError: ErrorCallbackType) {
        const q = new Query(`
            query {
                deliveryItems {
                    id
                    name
                    notAvailableOnWednesday
                }
            }
        `)
        q.onSuccess = (data: any) => {
            onSuccess(data.deliveryItems ? data.deliveryItems : [])
        }
        q.onError = onError
        q.execute()
    }

    static getDeliverySlots(
        dateFrom: Date,
        daysNumber: number,
        startWithFirstAvailable: boolean,
        wednesdaysNotAvailable: boolean,
        onSuccess: {(data: Array<DeliverySlotType>):void},
        onError: ErrorCallbackType,
    ) {
        const q = new Query(`
            query deliverySlots(
                $dateFrom: Date,
                $daysNumber: Int,
                $startWithFirstAvailable: Boolean,
                $wednesdaysNotAvailable: Boolean,
            ) {
                deliverySlots(
                    dateFrom: $dateFrom,
                    daysNumber: $daysNumber,
                    startWithFirstAvailable: $startWithFirstAvailable,
                    wednesdaysNotAvailable: $wednesdaysNotAvailable,
                ) {
                    id
                    date
                    startTime
                    endTime
                    outOfCapacity
                    notAvailable
                }
            }
        `, {
            dateFrom: getDateISOString(dateFrom),
            daysNumber: daysNumber,
            startWithFirstAvailable: startWithFirstAvailable,
            wednesdaysNotAvailable: wednesdaysNotAvailable,
        })
        q.onSuccess = (data: any) => {
            onSuccess(data.deliverySlots ? data.deliverySlots : [])
        }
        q.onError = onError
        q.execute()
    }

    static ERR_SLOT_NOT_AVAILABLE = 'slot_not_available'
    static ERR_ITEMS_NOT_EXIST = 'items_not_exist'
    static createDelivery(
        slotId: string,
        itemsIds: string[],
        onSuccess: {(data: DeliveryType):void},
        onError: ErrorCallbackType,
    ) {
        const q = new Query(`
            mutation createDelivery($data: CreateDeliveryDataType!) {
                createDelivery(data: $data) {
                    delivery {
                        id
                        slot {
                            outOfCapacity
                        }
                    }
                    errors {
                        code
                    }
                }
            }
        `, {data: {slotId: slotId, itemsIds: itemsIds}})
        q.onSuccess = (data: any) => {
            const errors = data['createDelivery']['errors']
            if (errors && onError) {
                const errCode = errors[0]?.code
                if (['001', '004', '005', '006'].includes(errCode)) { onError(ApiClient.ERR_SLOT_NOT_AVAILABLE) }
                else if (['003'].includes(errCode)) { onError(ApiClient.ERR_ITEMS_NOT_EXIST) }
                else {onError('Unknown error')}
            }
            else { onSuccess(data['createDelivery']['delivery']) }
        }
        q.onError = (error: any) => { console.log(error) }
        q.execute()
    }
}


export type DeliveryItemType = { id: string, name: string, notAvailableOnWednesday: boolean }
export type DeliverySlotType = {
    id: string,
    date: string,
    startTime: string,
    endTime: string,
    outOfCapacity: boolean,
    notAvailable: boolean,
}
export type DeliveryType = {
    slot: {outOfCapacity: boolean},
    id: string,
}

type ErrorCallbackType = { (error: string): void } | null
