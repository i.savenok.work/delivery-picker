

export function isLastIndex(arr: any[], index: number): boolean { return index === arr.length - 1}
export function isLastIsNull(arr: any[]): boolean { return arr.at(-1) === null }
