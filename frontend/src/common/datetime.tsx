

export function addDays(date: Date, days: number): Date {
  const result = new Date(date);
  result.setDate(result.getDate() + days);
  return result;
}


export function getDateISOString(date: Date): string {
    return date.toISOString().slice(0, 10)
}


export function getWeekdayBeautyString(date: Date) {return date.toLocaleDateString("en-GB", {weekday: 'long'})}
export function getDateBeautyString(date: Date) {return date.toLocaleDateString("en-GB", {year: 'numeric', month: 'short', day: 'numeric' })}
