

export function forcePush(obj: {[key: (string|number)]: Array<any>}, key: (string|number), item: any) {
    if (key in obj) {obj[key].push(item)} else {obj[key] = [item]}
}
