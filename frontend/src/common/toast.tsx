import { toast } from 'react-toastify';


const DEFAULT_TOAST_OPTIONS = {
    pauseOnHover: true,
    draggable: false,
    progress: undefined,
}


export class Toaster {
    static success = (msg: string) => { toast.success(msg, DEFAULT_TOAST_OPTIONS) }
    static info = (msg: string) => { toast.info(msg, DEFAULT_TOAST_OPTIONS) }
    static warn = (msg: string) => { toast.warning(msg, DEFAULT_TOAST_OPTIONS) }
    static error = (msg: string) => { toast.error(msg, DEFAULT_TOAST_OPTIONS) }
}
