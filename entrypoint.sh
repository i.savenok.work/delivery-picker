#!/bin/sh

if [ "$DATABASE" = "mysql" ]
then
    echo "Waiting for mysql..."

    while ! nc -z $DB_HOST $DB_PORT; do
      sleep 0.1
    done

    echo "MySQL started"
fi

echo "applying migrations..."
python manage.py migrate
echo "seeding dev data..."
python manage.py loaddata init_dev_data
echo "collecting static..."
python manage.py collectstatic --noinput

exec "$@"
