pytest_plugins = [
    'common.tests.fixtures',
    'entities.delivery_item.tests.fixtures',
    'entities.delivery_slot.tests.fixtures',
    'entities.delivery.tests.fixtures',
]
