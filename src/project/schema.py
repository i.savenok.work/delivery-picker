from common.graphene import Schema
from common.health import HealthQuery
from entities.delivery.schema import DeliveryMutation
from entities.delivery_item.schema import DeliveryItemQuery
from entities.delivery_slot.schema import DeliverySlotQuery


class Query(
    HealthQuery,
    DeliveryItemQuery,
    DeliverySlotQuery,
):
    pass


class Mutation(DeliveryMutation):
    pass


schema = Schema(
    query=Query,  # type: ignore
    mutation=Mutation,
)
