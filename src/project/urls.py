from graphene_django.views import GraphQLView

from common.admin import admin
from common.crsf import csrf_exempt
from common.urls import path
from project.settings import API_URL_PREFIX, GRAPHIQL

urlpatterns = [
    path('admin/', admin.site.urls),
    path(API_URL_PREFIX, csrf_exempt(GraphQLView.as_view(graphiql=GRAPHIQL))),
]
