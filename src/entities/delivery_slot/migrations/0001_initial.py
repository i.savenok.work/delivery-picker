# Generated by Django 3.0 on 2021-12-25 09:19

import uuid

from django.db import migrations

import common.models
import common.validators


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='BaseDeliverySlot',
            fields=[
                ('id', common.models.UUIDField(db_index=True, default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('created_at', common.models.DateTimeField(auto_now_add=True)),
                ('updated_at', common.models.DateTimeField(auto_now=True)),
                ('deleted_at', common.models.DateTimeField(blank=True, null=True)),
                ('start_time', common.models.TimeField()),
                ('end_time', common.models.TimeField()),
                ('max_capacity', common.models.PositiveSmallIntegerField(validators=[common.validators.MinValueValidator(1)])),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='DeliverySlot',
            fields=[
                ('id', common.models.UUIDField(db_index=True, default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('created_at', common.models.DateTimeField(auto_now_add=True)),
                ('updated_at', common.models.DateTimeField(auto_now=True)),
                ('deleted_at', common.models.DateTimeField(blank=True, null=True)),
                ('date', common.models.DateField()),
                ('start_time', common.models.TimeField()),
                ('end_time', common.models.TimeField()),
                ('max_capacity', common.models.PositiveSmallIntegerField(validators=[common.validators.MinValueValidator(1)])),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
