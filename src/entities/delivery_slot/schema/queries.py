from typing import Optional

from common.datetime import add_date, date, is_friday, is_odd_week_in_year
from common.graphene import Boolean, Date, Int, List, ObjectType
from common.models import QuerySet
from entities.delivery_slot.models import DeliverySlot
from entities.delivery_slot.schema.types import DeliverySlotType


class DeliverySlotQuery(ObjectType):
    delivery_slots = List(
        DeliverySlotType,
        date_from=Date(),
        days_number=Int(),
        start_with_first_available=Boolean(),
        wednesdays_not_available=Boolean(),
    )

    def resolve_delivery_slots(
        self,
        info,
        date_from: date = None,
        days_number: int = 4,
        start_with_first_available: bool = False,
        wednesdays_not_available: bool = False,  # only hint for "start_with_first_available"
    ) -> Optional[QuerySet]:
        date_from = date_from or date.today()

        if start_with_first_available:
            if wednesdays_not_available:
                closest_available_slot = DeliverySlot.objects.exclude_wednesdays().get_closest_available(date=date_from)
            else:
                closest_available_slot = DeliverySlot.objects.get_closest_available(date=date_from)

            if not closest_available_slot:
                return None

            date_from = closest_available_slot.date

        date_to = add_date(date_from, days=days_number-1)
        delivery_slots = DeliverySlot.objects.in_dates_range(
            date_from=date_from,
            date_to=date_to,
        ).order_by(
            'date', 'start_time'
        ).prefetch_related(
            'deliveries'
        ).all()

        # set "not_available" attribute to avoid unnecessary SQL queries in DeliverySlotType resolver
        for slot in delivery_slots:
            slot.not_available = False

            if is_friday(slot.date) and is_odd_week_in_year(slot.date):
                try:
                    next(  # search for previous slots for slot date in fetched queryset
                        filter(
                            lambda other_slot: other_slot.date == slot.date and other_slot.end_time <= slot.start_time,
                            delivery_slots,
                        )
                    )
                    continue
                except StopIteration:
                    slot.not_available = True

        return delivery_slots or None
