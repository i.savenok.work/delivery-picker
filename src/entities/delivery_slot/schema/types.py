from common.graphene import Boolean, Node
from entities.delivery_slot.models import DeliverySlot


class DeliverySlotType(Node):
    class Meta:
        model = DeliverySlot
        fields = (
            'id',
            'date',
            'start_time',
            'end_time',
            'out_of_capacity',
            'not_available',
        )

    out_of_capacity = Boolean()
    not_available = Boolean()  # should be resolved in query resolver

    def resolve_out_of_capacity(self: DeliverySlot, info):
        return self.get_deliveries_capacity() < 1
