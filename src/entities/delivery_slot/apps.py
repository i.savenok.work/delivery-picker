from common.app_config import AppConfig


class DeliverySlotConfig(AppConfig):
    name = 'entities.delivery_slot'
