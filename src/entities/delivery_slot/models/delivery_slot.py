from typing import TYPE_CHECKING, Optional

from common.datetime import is_friday, is_odd_week_in_year
from common.validators import MinValueValidator

if TYPE_CHECKING:
    from entities.delivery.models import DeliveryManager

from common import datetime, models
from common.models import Count, F, Q


class DeliverySlotQuerySet(models.QuerySet):

    def earlier_for_this_date(self, date: datetime.date, time: datetime.time) -> models.QuerySet:
        return self.filter(Q(date=date) & Q(end_time__lte=time))

    def latest(self) -> Optional['DeliverySlot']:
        return self.order_by('-date', '-end_time').first()

    def exclude_wednesdays(self) -> models.QuerySet:
        return self.exclude(date__week_day=4)

    def closest_with_free_capacity(self, date: datetime.date) -> models.QuerySet:
        return self.filter(
            date__gte=date
        ).order_by(
            'date', 'start_time'
        ).annotate(
            scheduled_deliveries_number=Count('deliveries')
        ).filter(
            scheduled_deliveries_number__lt=F('max_capacity')
        )

    def in_dates_range(self, date_from: datetime.date, date_to: datetime.date) -> models.QuerySet:
        return self.filter(
            Q(date__gte=date_from)
            &
            Q(date__lte=date_to)
        )

    def get_closest_available(self, date: datetime.date) -> Optional['DeliverySlot']:
        slots_with_free_capacity = self.closest_with_free_capacity(date).all()[:2]
        # searching for 2 closest future slots with free capacity

        try:
            first_slot: 'DeliverySlot' = slots_with_free_capacity[0]

            if is_friday(first_slot.date) and is_odd_week_in_year(first_slot.date):
                # "first_slot" is the first in Friday of the odd week of the year.
                # Checking for  the presence of  previous slots on this day is not
                # necessary,  because such slots always have free capacity and for
                # the date will always be the earliest with free capacity.

                second_slot: 'DeliverySlot' = slots_with_free_capacity[1]
                if first_slot.date != second_slot.date:
                    # "first_slot" is the only free slot in day (next free slot have other date)
                    # quite rare case

                    if not is_friday(second_slot.date) and not is_odd_week_in_year(second_slot.date):
                        # "second_slot" is not the first for Friday of the odd week of the year, returning it.
                        return second_slot
                    else:
                        # Same as "first_slot" - "second_slot" is the first in Friday of the odd week of the year.
                        # Retrying in recursion with date with "second_slot" date to check if it is  the only free
                        # slot in day.
                        return self.get_closest_available(second_slot.date)

                else:
                    return second_slot  # "second_slot" is next after first in Friday of the odd week

            else:
                return first_slot

        except IndexError:
            return None  # there are no any free slots in future


class DeliverySlotManager(models.Manager):

    def create(
        self,
        date: datetime.date,
        start_time: datetime.time,
        end_time: datetime.time,
        max_capacity: int,
    ) -> 'DeliverySlot':
        return super(DeliverySlotManager, self).create(
            date=date,
            start_time=start_time,
            end_time=end_time,
            max_capacity=max_capacity,
        )


class DeliverySlot(models.Model):
    date: datetime.date = models.DateField(null=False)
    start_time: datetime.time = models.TimeField(null=False)
    end_time: datetime.time = models.TimeField(null=False)
    max_capacity: int = models.PositiveSmallIntegerField(null=False, validators=[MinValueValidator(1)])

    deliveries: 'DeliveryManager'

    objects = DeliverySlotManager.from_queryset(DeliverySlotQuerySet)()

    def get_deliveries_capacity(self) -> int:
        current_deliveries_number = self.deliveries.count()
        return self.max_capacity - current_deliveries_number

    def is_first_in_day(self):
        return not DeliverySlot.objects.earlier_for_this_date(date=self.date, time=self.start_time).exists()

    def is_first_for_friday_of_the_odd_week_of_the_year(self):
        return all((
            is_friday(self.date),
            is_odd_week_in_year(self.date),
            self.is_first_in_day(),
        ))

    def __str__(self) -> str:
        return self.short_id
