from typing import Optional

from common import datetime, models
from common.exceptions import ValidationError
from common.models import Q
from common.validators import MinValueValidator


class BaseDeliverySlotManager(models.Manager):

    def create(
        self,
        start_time: datetime.time,
        end_time: datetime.time,
        max_capacity: int,
    ) -> 'BaseDeliverySlot':
        return super(BaseDeliverySlotManager, self).create(
            start_time=start_time,
            end_time=end_time,
            max_capacity=max_capacity,
        )

    def get_intersection_or_none(
        self,
        start_time: datetime.time,
        end_time: datetime.time,
    ) -> Optional['BaseDeliverySlot']:
        return self.filter(
            (Q(start_time__lte=end_time) & Q(end_time__gte=start_time))
            |
            (Q(start_time__gte=end_time) & Q(end_time__lte=start_time))
        ).first()


class BaseDeliverySlot(models.Model):
    start_time: datetime.time = models.TimeField(null=False)
    end_time: datetime.time = models.TimeField(null=False)
    max_capacity = models.PositiveSmallIntegerField(null=False, validators=[MinValueValidator(1)])

    objects = BaseDeliverySlotManager()

    def validate(self):
        if self.start_time >= self.end_time:
            raise InvalidTimeInterval('End time must be greater than start time')

        intersecting_slot = BaseDeliverySlot.objects.get_intersection_or_none(
            start_time=self.start_time,
            end_time=self.end_time,
        )
        if intersecting_slot and intersecting_slot.id != self.id:
            raise SlotIntersectsOtherSlot(f'Selected time interval is intersecting existing slot {intersecting_slot}')

    def __str__(self) -> str:
        return self.short_id


class InvalidTimeInterval(ValidationError):
    pass


class SlotIntersectsOtherSlot(ValidationError):
    pass
