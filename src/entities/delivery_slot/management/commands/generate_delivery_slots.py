from common.datetime import add_date, date, timedelta
from common.exceptions import CommandError
from common.management import BaseCommand, CommandParser
from entities.delivery_slot.models import BaseDeliverySlot, DeliverySlot


class Command(BaseCommand):

    def add_arguments(self, parser: 'CommandParser'):
        parser.add_argument('--weeks_num', type=int, default=4)

    def handle(self, *args, **options):
        days_num = options.get('weeks_num', 4) * 7

        last_delivery_slot = DeliverySlot.objects.latest()
        if last_delivery_slot:
            start_date: date = add_date(last_delivery_slot.date, days=1)
        else:
            start_date: date = date.today()

        base_delivery_slots = BaseDeliverySlot.objects.all()
        if not base_delivery_slots:
            raise CommandError('No base delivery slots found')

        delivery_slots_to_create = []
        for diff_of_days in range(days_num):
            delivery_slot_date = start_date + timedelta(days=diff_of_days)

            for base_delivery_slot in base_delivery_slots:
                delivery_slots_to_create.append(
                    DeliverySlot(
                        date=delivery_slot_date,
                        start_time=base_delivery_slot.start_time,
                        end_time=base_delivery_slot.end_time,
                        max_capacity=base_delivery_slot.max_capacity,
                    )
                )

        DeliverySlot.objects.bulk_create(delivery_slots_to_create)
