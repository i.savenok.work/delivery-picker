import pytest

from common.datetime import add_date, add_time


@pytest.mark.django_db
class TestDeliveryItemQuery:

    def test_get_delivery_slots(self, fake, execute_graphql_query, delivery_slot_factory, delivery_factory):
        odd_week_wednesday = fake.datetime(year=2021, month=12, day=22).date()
        odd_week_friday = add_date(odd_week_wednesday, days=2)

        dates_range = [odd_week_wednesday, odd_week_friday]
        # in range
        slot_1 = delivery_slot_factory(date=dates_range[0], max_capacity=1)
        delivery_factory(slot=slot_1)  # now slot_1 should be out of capacity

        slot_2 = delivery_slot_factory(date=add_date(dates_range[0], days=1))
        slot_3 = delivery_slot_factory(date=dates_range[1])  # odd week friday first slot
        slot_4 = delivery_slot_factory(
            date=dates_range[1],
            start_time=add_time(slot_3.start_time, hours=1),
        )  # odd week friday second slot

        # out of range
        delivery_slot_factory(date=add_date(dates_range[0], days=-1))
        delivery_slot_factory(date=add_date(dates_range[1], days=1))

        response = execute_graphql_query("""
            query {
                deliverySlots (
                    dateFrom: \"""" + dates_range[0].isoformat() + """\",
                    daysNumber: 3,
                ) { id, date, startTime, endTime, outOfCapacity, notAvailable }
            }
        """)
        assert response['data']['deliverySlots'] == [
            {
                'id': slot.short_id,
                'date': slot.date.isoformat(),
                'startTime': slot.start_time.isoformat(),
                'endTime': slot.end_time.isoformat(),
                'outOfCapacity': slot.get_deliveries_capacity() == 0,
                'notAvailable': slot.is_first_for_friday_of_the_odd_week_of_the_year(),
            }
            for slot in [slot_1, slot_2, slot_3, slot_4]
        ]

    def test_get_delivery_slots_starting_with_first_available(
        self,
        fake,
        execute_graphql_query,
        delivery_slot_factory,
        delivery_factory,
    ):
        odd_week_friday = fake.datetime(year=2021, month=12, day=24).date()
        dates_range = [odd_week_friday, add_date(odd_week_friday, 3)]

        delivery_slot_factory(date=add_date(odd_week_friday, -1))
        # slot 1 is before dateFrom

        slot_2 = delivery_slot_factory(date=odd_week_friday)
        # slot_2 is first in Friday of the odd week

        slot_3 = delivery_slot_factory(date=odd_week_friday, max_capacity=1, start_time=add_time(slot_2.start_time, 1))
        delivery_factory(slot=slot_3)
        # slot_3 is second in Friday of the odd week but it is out of capacity

        slot_4 = delivery_slot_factory(date=add_date(odd_week_friday, +1), max_capacity=1)
        delivery_factory(slot=slot_4)
        # slot_4 is out of capacity

        delivery_slot_factory(date=add_date(odd_week_friday, 14), max_capacity=1)
        # slot 5 is first and the only in next Friday of the odd week

        delivery_slot_factory(date=add_date(odd_week_friday, 26), max_capacity=1)
        # slot 5.5 is wednesday slot

        slot_6 = delivery_slot_factory(date=add_date(odd_week_friday, 28), max_capacity=1)
        slot_7 = delivery_slot_factory(date=slot_6.date, max_capacity=1, start_time=add_time(slot_6.start_time, 1))
        # slot_6 is the first in next next Friday of the odd week, slot_7 is the second

        slot_8 = delivery_slot_factory(date=add_date(slot_6.date, 1))
        # slot_8 goes after slot_7, available
        slot_9 = delivery_slot_factory(
            date=add_date(slot_6.date, 1),
            start_time=add_time(slot_8.start_time, 1),
            max_capacity=1,
        )
        delivery_factory(slot=slot_8)
        # slot_9 is in slot_8 date but out of capacity
        delivery_slot_factory(date=add_date(slot_6.date, 10))
        # slot 10 goes in 10 days after slot_6

        response = execute_graphql_query("""
            query {
                deliverySlots (
                    dateFrom: \"""" + dates_range[0].isoformat() + """\",
                    daysNumber: 3,
                    startWithFirstAvailable: true,
                    wednesdaysNotAvailable: true,
                ) { id }
            }
        """)
        assert response['data']['deliverySlots'] == [
            {'id': slot.short_id}
            for slot in
            [
                slot_6, slot_7,  # closest date with available slots
                slot_8, slot_9,  # next date after first with available slots
            ]
        ]

    def test_get_delivery_slots_starting_with_slot_with_free_capacity(
        self,
        fake,
        execute_graphql_query,
        delivery_slot_factory,
        delivery_factory,
    ):
        dates_range = [fake.datetime(day=22).date(), fake.datetime(day=25).date()]

        slot_1 = delivery_slot_factory(date=dates_range[0], max_capacity=1)
        delivery_factory(slot=slot_1)  # in range, out of capacity

        slot_2 = delivery_slot_factory(date=dates_range[1])  # in range

        # out of range
        delivery_slot_factory(date=add_date(dates_range[0], days=-1))
        slot_6 = delivery_slot_factory(date=add_date(dates_range[1], days=1))

        response = execute_graphql_query("""
            query {
                deliverySlots (
                    dateFrom: \"""" + dates_range[0].isoformat() + """\",
                    daysNumber: 3,
                    startWithFirstAvailable: true,
                ) { id }
            }
        """)
        assert response['data']['deliverySlots'] == [
            {'id': slot.short_id}
            for slot in
            [slot_2, slot_6]
        ]

    def test_try_to_get_delivery_slots_starting_with_slot_with_free_capacity_if_not_exists(
        self,
        execute_graphql_query,
        fake,
    ):
        response = execute_graphql_query("""
            query {
                deliverySlots (
                    dateFrom: \"""" + fake.datetime().date().isoformat() + """\",
                    daysNumber: 3,
                    startWithFirstAvailable: true,
                ) { id }
            }
        """)
        assert response['data']['deliverySlots'] is None
