import pytest

from common.datetime import add_date, add_time, datetime, timedelta
from common.exceptions import DataError
from entities.delivery_slot.models import BaseDeliverySlot, DeliverySlot
from entities.delivery_slot.models.base_delivery_slot import (
    InvalidTimeInterval,
    SlotIntersectsOtherSlot
)


@pytest.mark.django_db
class TestBaseDeliverySlot:

    def test_create(self, fake):
        start_time, end_time = fake.datetime(hour=11).time(), fake.datetime(hour=12).time()
        capacity = fake.pyint(1, 5)
        base_slot = BaseDeliverySlot.objects.create(
            start_time=start_time, end_time=end_time, max_capacity=capacity
        )
        assert isinstance(base_slot, BaseDeliverySlot)
        assert base_slot.start_time == start_time
        assert base_slot.end_time == end_time
        assert base_slot.max_capacity == capacity

    def test_update(self, base_delivery_slot_factory, fake):
        slot = base_delivery_slot_factory(max_capacity=2)
        slot.max_capacity = 1
        new_end_time = add_time(slot.end_time, hours=1)
        slot.end_time = new_end_time
        slot.save()
        assert slot.max_capacity == 1
        assert slot.end_time == new_end_time

    def test_try_to_set_wrong_ordered_time(self, fake, base_delivery_slot_factory):
        slot = base_delivery_slot_factory()
        slot.start_time = fake.datetime(hour=12).time()
        slot.end_time = fake.datetime(hour=11).time()
        with pytest.raises(InvalidTimeInterval):
            slot.save()

    def test_try_to_set_invalid_capacity(self, base_delivery_slot_factory):
        with pytest.raises(DataError):
            base_delivery_slot_factory(max_capacity=-1)

    def test_check_are_intersections_exist(self, fake, base_delivery_slot_factory):
        slot = base_delivery_slot_factory(
            start_time=fake.datetime(hour=10).time(),
            end_time=fake.datetime(hour=14).time(),
        )
        intersecting_intervals = [
            (add_time(slot.start_time, hours=-1), add_time(slot.end_time, hours=1)),
            (slot.start_time, add_time(slot.end_time, hours=1)),
            (add_time(slot.start_time, hours=1), add_time(slot.end_time, hours=-1)),
            (add_time(slot.start_time, hours=1), add_time(slot.end_time, hours=1)),
            (add_time(slot.start_time, hours=-1), add_time(slot.end_time, hours=-1)),
            (add_time(slot.start_time, hours=-1), slot.end_time),
        ]
        for interval in intersecting_intervals:
            assert BaseDeliverySlot.objects.get_intersection_or_none(interval[0], interval[1])

        not_intersecting_intervals = [
            (add_time(slot.start_time, hours=-2), add_time(slot.start_time, hours=-1)),
            (add_time(slot.end_time, hours=1), add_time(slot.end_time, hours=2)),
        ]

        for interval in not_intersecting_intervals:
            assert not BaseDeliverySlot.objects.get_intersection_or_none(interval[0], interval[1])

    def test_try_to_set_intersecting_time_interval(self, fake, base_delivery_slot_factory):
        slot_1 = base_delivery_slot_factory()
        with pytest.raises(SlotIntersectsOtherSlot):
            base_delivery_slot_factory(
                start_time=slot_1.start_time,
                end_time=slot_1.end_time,
            )


@pytest.mark.django_db
class TestDeliverySlot:

    def test_create(self, fake):
        date = fake.datetime().date()
        start_time, end_time = fake.datetime(hour=11).time(), fake.datetime(hour=12).time()
        capacity = fake.pyint(1, 5)
        slot = DeliverySlot.objects.create(
            date=date, start_time=start_time, end_time=end_time, max_capacity=capacity
        )
        assert isinstance(slot, DeliverySlot)
        assert slot.date == date
        assert slot.start_time == start_time
        assert slot.end_time == end_time
        assert slot.max_capacity == capacity

    def test_update(self, delivery_slot_factory, fake):
        slot = delivery_slot_factory()
        slot.max_capacity = 1
        new_end_time = add_time(slot.end_time, hours=1)
        slot.end_time = new_end_time
        slot.save()
        assert slot.max_capacity == 1
        assert slot.end_time == new_end_time

    def test_get_deliveries_capacity(self, delivery_slot_factory, delivery_factory):
        slot = delivery_slot_factory(max_capacity=5)
        assert slot.get_deliveries_capacity() == 5

        delivery_factory(slot=slot)
        slot.refresh_from_db()
        assert slot.get_deliveries_capacity() == 4

    def test_define_if_earlier_slot_for_the_date_exists(self, fake, delivery_slot_factory):
        date_and_time_to_check = datetime(year=2021, month=12, day=21, hour=11)
        assert not DeliverySlot.objects.earlier_for_this_date(
            date=date_and_time_to_check.date(),
            time=date_and_time_to_check.time(),
        ).exists()

        same_date_but_in_hour = date_and_time_to_check + timedelta(hours=1)
        delivery_slot_factory(
            date=same_date_but_in_hour.date(),
            start_time=same_date_but_in_hour.time(),
        )
        assert not DeliverySlot.objects.earlier_for_this_date(
            date=date_and_time_to_check.date(),
            time=date_and_time_to_check.time(),
        ).exists()

        previous_day_and_earlier_by_time = date_and_time_to_check - timedelta(days=1, hours=1)
        delivery_slot_factory(
            date=previous_day_and_earlier_by_time.date(),
            start_time=(previous_day_and_earlier_by_time - timedelta(hours=1)).time(),
            end_time=previous_day_and_earlier_by_time.time(),
        )
        assert not DeliverySlot.objects.earlier_for_this_date(
            date=date_and_time_to_check.date(),
            time=date_and_time_to_check.time(),
        ).exists()

        same_day_and_earlier_by_time = date_and_time_to_check - timedelta(hours=1)
        delivery_slot_factory(
            date=same_day_and_earlier_by_time.date(),
            start_time=(same_day_and_earlier_by_time - timedelta(hours=1)).time(),
            end_time=same_day_and_earlier_by_time.time(),
        )
        assert DeliverySlot.objects.earlier_for_this_date(
            date=date_and_time_to_check.date(),
            time=date_and_time_to_check.time(),
        ).exists()

    def test_get_closest_with_free_capacity(self, fake, delivery_slot_factory, delivery_factory):
        lookup_date = fake.datetime(day=2).date()
        lookup_slot = delivery_slot_factory(max_capacity=1, date=add_date(lookup_date, days=+2))

        delivery_slot_factory(max_capacity=1, date=add_date(lookup_date, days=-1))  # free slot, but in past
        delivery_slot_factory(max_capacity=1, date=add_date(lookup_date, days=+3))  # free slot, but not closest
        delivery_factory(slot=delivery_slot_factory(max_capacity=1, date=lookup_date))  # out of capacity slot

        slot = DeliverySlot.objects.closest_with_free_capacity(lookup_date).first()
        assert slot == lookup_slot

    def test_get_slots_in_date_range(self, fake, delivery_slot_factory):
        dates_range = [fake.datetime(day=3).date(), fake.datetime(day=5).date()]
        slots_in_range = {
            delivery_slot_factory(date=dates_range[0]),
            delivery_slot_factory(date=add_date(dates_range[0], days=1)),
            delivery_slot_factory(date=dates_range[1]),
            delivery_slot_factory(date=dates_range[1]),
        }

        # slots out of range:
        delivery_slot_factory(date=add_date(dates_range[0], days=-1))
        delivery_slot_factory(date=add_date(dates_range[1], days=+1))

        assert set(
            DeliverySlot.objects.in_dates_range(date_from=dates_range[0], date_to=dates_range[1]).all()
        ) == slots_in_range

    def test_get_closest_available(self, fake, delivery_slot_factory, delivery_factory):
        odd_week_friday = fake.datetime(year=2021, month=12, day=24).date()

        delivery_slot_factory(date=add_date(odd_week_friday, days=-1), max_capacity=1)
        # this slot is available but before lookup_date
        assert DeliverySlot.objects.get_closest_available(odd_week_friday) is None

        slot_2 = delivery_slot_factory(date=odd_week_friday, max_capacity=1)
        # slot_2 is the first in friday of the odd week of the year
        slot_3 = delivery_slot_factory(
            date=odd_week_friday,
            max_capacity=1,
            start_time=add_time(slot_2.start_time, hours=1),
        )
        # slot_3 is the second in friday of the odd week of the year
        assert DeliverySlot.objects.get_closest_available(odd_week_friday) == slot_3

        delivery_factory(slot=slot_3)
        # now slot_3 is out of capacity
        assert DeliverySlot.objects.get_closest_available(odd_week_friday) is None

        delivery_slot_factory(date=add_date(odd_week_friday, days=14), max_capacity=1)
        # slot 4 is the first in next friday of the odd week of the year
        assert DeliverySlot.objects.get_closest_available(odd_week_friday) is None

        slot_5 = delivery_slot_factory(date=add_date(odd_week_friday, days=21), max_capacity=1)
        # slot_5 is the first and only in friday of the even week of the year
        assert DeliverySlot.objects.get_closest_available(odd_week_friday) == slot_5

        delivery_slot_factory(date=add_date(odd_week_friday, days=22), max_capacity=1)
        # slot_6 is available but not the closest one
        assert DeliverySlot.objects.get_closest_available(odd_week_friday) == slot_5

        slot_7 = delivery_slot_factory(date=add_date(odd_week_friday, days=2), max_capacity=1)
        delivery_factory(slot=slot_7)
        # slot_7 is closer than slot_5 but it is out of capacity
        assert DeliverySlot.objects.get_closest_available(odd_week_friday) == slot_5

    def test_exclude_wednesdays(self, fake, delivery_slot_factory):
        wednesday_delivery_slot = delivery_slot_factory(date=fake.datetime(iso_weekday=3))
        assert wednesday_delivery_slot in DeliverySlot.objects.all()
        assert wednesday_delivery_slot not in DeliverySlot.objects.exclude_wednesdays().all()
