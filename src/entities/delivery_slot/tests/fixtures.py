import pytest

from common.datetime import add_time
from common.datetime import date as datetime_date
from common.datetime import time
from entities.delivery_slot.models import BaseDeliverySlot, DeliverySlot


@pytest.fixture
@pytest.mark.django_db
def base_delivery_slot_factory(fake):
    def create(start_time: time = None, end_time: time = None, max_capacity: int = None) -> 'BaseDeliverySlot':
        slot = BaseDeliverySlot.objects.create(
            start_time=start_time or fake.datetime(hour=11).time(),
            end_time=end_time or fake.datetime(hour=12).time(),
            max_capacity=max_capacity if max_capacity is not None else 3,
        )
        return slot
    return create


@pytest.fixture
@pytest.mark.django_db
def delivery_slot_factory(fake):
    def create(
        date: datetime_date = None,
        start_time: time = None,
        end_time: time = None,
        max_capacity: int = None,
    ) -> 'DeliverySlot':
        start_time = start_time or fake.datetime(hour=11).time()
        slot = DeliverySlot.objects.create(
            date=date or fake.datetime().date(),
            start_time=start_time,
            end_time=end_time or add_time(start_time, hours=1),
            max_capacity=max_capacity if max_capacity is not None else 3,
        )
        return slot
    return create
