import pytest

from common.datetime import add_date, add_time, date
from common.exceptions import CommandError
from common.management import call_command
from entities.delivery_slot.management.commands import GENERATE_DELIVERY_SLOTS
from entities.delivery_slot.models import BaseDeliverySlot, DeliverySlot


@pytest.mark.django_db
class TestDeliverySlotGeneration:

    def test_try_to_generate_without_base_slots(self):
        BaseDeliverySlot.objects.all().delete()
        with pytest.raises(CommandError):
            call_command(GENERATE_DELIVERY_SLOTS)

    def test_generate_for_week(self, base_delivery_slot_factory):
        DeliverySlot.objects.all().delete()

        base_slot_1 = base_delivery_slot_factory()
        base_slot_2 = base_delivery_slot_factory(
            start_time=add_time(base_slot_1.end_time, hours=1),
            end_time=add_time(base_slot_1.end_time, hours=2),
            max_capacity=base_slot_1.max_capacity + 1,
        )

        call_command(GENERATE_DELIVERY_SLOTS, weeks_num=1)

        assert DeliverySlot.objects.count() == 7 * 2
        generated_slots = DeliverySlot.objects.order_by('date', 'start_time').all()

        expected_date = date.today()
        for slot_1, slot_2 in zip(generated_slots[::2], generated_slots[1::2]):

            assert slot_1.date == expected_date
            assert slot_1.start_time == base_slot_1.start_time
            assert slot_1.end_time == base_slot_1.end_time
            assert slot_1.max_capacity == base_slot_1.max_capacity
            assert slot_1.get_deliveries_capacity() == base_slot_1.max_capacity
            assert slot_1.is_first_in_day()

            assert slot_2.date == expected_date
            assert slot_2.start_time == base_slot_2.start_time
            assert slot_2.end_time == base_slot_2.end_time
            assert slot_2.max_capacity == base_slot_2.max_capacity
            assert slot_2.get_deliveries_capacity() == base_slot_2.max_capacity
            assert not slot_2.is_first_in_day()

            expected_date = add_date(expected_date, days=1)

    def test_generate_for_week_when_slots_exist(self, fake, base_delivery_slot_factory, delivery_slot_factory):
        DeliverySlot.objects.all().delete()
        BaseDeliverySlot.objects.all().delete()

        base_slot_1 = base_delivery_slot_factory(start_time=fake.datetime(year=0).time())
        existing_slot = delivery_slot_factory(
            date=fake.datetime(year=+1).date(),
            max_capacity=base_slot_1.max_capacity + 1,
        )

        call_command(GENERATE_DELIVERY_SLOTS, weeks_num=1)

        assert DeliverySlot.objects.count() == 1 + (7 * 1)
        generated_slots = list(DeliverySlot.objects.order_by('date', 'end_time').all())
        assert generated_slots[0] == existing_slot

        assert generated_slots[1].date == add_date(existing_slot.date, days=1)
        assert generated_slots[1].max_capacity == base_slot_1.max_capacity

        assert generated_slots[-1].date == add_date(existing_slot.date, days=7)
        assert generated_slots[-1].max_capacity == base_slot_1.max_capacity

    def test_generate_for_2_weeks(self, fake, base_delivery_slot_factory):
        DeliverySlot.objects.all().delete()
        BaseDeliverySlot.objects.all().delete()
        base_delivery_slot_factory()
        call_command(GENERATE_DELIVERY_SLOTS, weeks_num=2)
        assert DeliverySlot.objects.count() == 7 * 2
