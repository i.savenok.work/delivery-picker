from common import admin, messages
from common.exceptions import CommandError
from common.http import HttpResponseRedirect
from common.management import call_command
from common.urls import path
from entities.delivery_slot.management.commands import GENERATE_DELIVERY_SLOTS
from entities.delivery_slot.models import BaseDeliverySlot, DeliverySlot


class BaseDeliverySlotAdmin(admin.ModelAdmin):
    list_display = ('short_id', 'start_time', 'end_time', 'max_capacity')
    readonly_fields = ('created_at', 'updated_at', 'deleted_at')
    fields = ('start_time', 'end_time', 'max_capacity', 'created_at', 'updated_at', 'deleted_at')


class DeliverySlotAdmin(admin.ModelAdmin):
    list_display = ('short_id', 'date', 'start_time', 'end_time', 'max_capacity', 'get_current_capacity')
    readonly_fields = ('created_at', 'updated_at', 'deleted_at', 'get_current_capacity')
    fields = ('date', 'start_time', 'end_time', 'max_capacity', 'created_at', 'updated_at', 'deleted_at')
    ordering = ('-date', '-start_time',)

    def get_queryset(self, request):
        qs = super(DeliverySlotAdmin, self).get_queryset(request)
        return qs.prefetch_related('deliveries')

    def get_current_capacity(self, obj: DeliverySlot):
        return obj.get_deliveries_capacity()

    get_current_capacity.short_description = 'Current capacity'

    change_list_template = 'admin/change_delivery_slots_list.html'

    def get_urls(self):
        defaults = super().get_urls()
        return [path('generate_delivery_slots/', self.generate_delivery_slots)] + defaults

    def generate_delivery_slots(self, request):
        try:
            call_command(GENERATE_DELIVERY_SLOTS, weeks_num=4)
            self.message_user(
                request=request,
                message='Delivery slots generation completed successfully',
                level=messages.INFO,
            )
        except CommandError as err:
            self.message_user(
                request=request,
                message=f'Delivery slots generation failed, details: {err}',
                level=messages.ERROR,
            )
        return HttpResponseRedirect('../')


admin.site.register(BaseDeliverySlot, BaseDeliverySlotAdmin)
admin.site.register(DeliverySlot, DeliverySlotAdmin)
