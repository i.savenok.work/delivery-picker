from common import admin
from entities.delivery_item.models import DeliveryItem


class DeliveryItemAdmin(admin.ModelAdmin):
    search_fields = ('name',)
    list_display = ('name', 'not_available_on_wednesday', 'short_id')
    list_filter = ('not_available_on_wednesday',)
    readonly_fields = ('created_at', 'updated_at', 'deleted_at')
    fields = ('name', 'not_available_on_wednesday', 'created_at', 'updated_at', 'deleted_at')


admin.site.register(DeliveryItem, DeliveryItemAdmin)
