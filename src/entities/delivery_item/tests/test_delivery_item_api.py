import pytest


@pytest.mark.django_db
class TestDeliveryItemQuery:

    def test_get_delivery_items(self, execute_graphql_query, delivery_item_factory):
        item_1, item_2 = delivery_item_factory(), delivery_item_factory(not_available_on_wednesday=True)
        response = execute_graphql_query("""
            query {
                deliveryItems { id, name, notAvailableOnWednesday }
            }
        """)
        assert response['data']['deliveryItems'] == [
            {
                'id': item_1.short_id,
                'name': item_1.name,
                'notAvailableOnWednesday': item_1.not_available_on_wednesday,
            },
            {
                'id': item_2.short_id,
                'name': item_2.name,
                'notAvailableOnWednesday': item_2.not_available_on_wednesday,
            },
        ]

    def test_get_delivery_items_if_not_exist(self, execute_graphql_query):
        response = execute_graphql_query("""
            query {
                deliveryItems { id }
            }
        """)
        assert response == {'data': {'deliveryItems': []}}
