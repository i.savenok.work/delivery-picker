import pytest

from entities.delivery_item.models import DeliveryItem


@pytest.fixture
@pytest.mark.django_db
def delivery_item_factory(fake):
    def create(name: str = None, not_available_on_wednesday: bool = None):
        item = DeliveryItem.objects.create(
            name=name or fake.unique.word(),
            not_available_on_wednesday=not_available_on_wednesday or False
        )
        return item
    return create
