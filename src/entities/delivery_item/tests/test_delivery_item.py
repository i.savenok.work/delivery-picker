import pytest

from common import exceptions
from entities.delivery_item.models import (
    CantBeUnavailableForWednesdays,
    DeliveryItem
)


@pytest.mark.django_db
class TestDeliveryItem:

    def test_create_delivery_item(self, fake):
        name = fake.unique.word()
        item = DeliveryItem.objects.create(name=name)
        assert isinstance(item, DeliveryItem)
        assert item.name == name == str(item)
        assert not item.not_available_on_wednesday

    def test_create_not_available_on_wednesday_delivery_item(self, fake):
        item = DeliveryItem.objects.create(name=fake.unique.word(), not_available_on_wednesday=True)
        assert item.not_available_on_wednesday

    def test_try_to_create_with_duplicate_name(self, delivery_item_factory):
        item = delivery_item_factory()
        with pytest.raises(exceptions.IntegrityError):
            DeliveryItem.objects.create(name=item.name)

    def test_update_delivery_item(self, fake, delivery_item_factory):
        name_1, name_2 = fake.unique.word(), fake.unique.word()
        item = delivery_item_factory(name=name_1)
        assert item.name == name_1
        assert not item.not_available_on_wednesday
        item.name = name_2
        item.not_available_on_wednesday = True
        item.save()
        assert item.name == name_2
        assert item.not_available_on_wednesday

    def test_try_to_set_item_as_unavailable_on_wednesday_when_delivery_is_scheduled(
        self,
        fake,
        delivery_item_factory,
        delivery_factory,
        delivery_slot_factory,
    ):
        item = delivery_item_factory(not_available_on_wednesday=False)
        next_wednesday_date = fake.datetime(year=+1, iso_weekday=3)
        next_wednesday_slot = delivery_slot_factory(date=next_wednesday_date)
        delivery_factory(slot=next_wednesday_slot, items=[item])

        with pytest.raises(CantBeUnavailableForWednesdays):
            item.not_available_on_wednesday = True
            item.save()
