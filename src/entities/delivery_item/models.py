from typing import TYPE_CHECKING

from common.exceptions import ValidationError

if TYPE_CHECKING:
    from entities.delivery.models import DeliveryManager

from common import models


class DeliveryItemManager(models.Manager):

    def create(self, name: str, not_available_on_wednesday: bool = False) -> 'DeliveryItem':
        return super(DeliveryItemManager, self).create(name=name, not_available_on_wednesday=not_available_on_wednesday)


class DeliveryItem(models.Model):
    name: str = models.CharField(max_length=128, null=False, blank=False, unique=True)
    not_available_on_wednesday: bool = models.BooleanField(default=False)

    objects = DeliveryItemManager()

    deliveries: 'DeliveryManager'

    def validate(self):
        if self.not_available_on_wednesday:
            scheduled_deliveries_on_wednesdays = self.deliveries.get_future_wednesday_deliveries()
            if scheduled_deliveries_on_wednesdays:
                raise CantBeUnavailableForWednesdays(
                    f'Can\'t be marked as unavailable for wednesdays, because of scheduled wednesday deliveries: '
                    f'{", ".join([str(delivery) for delivery in scheduled_deliveries_on_wednesdays])}'
                )

    def __str__(self) -> str:
        return self.name


class CantBeUnavailableForWednesdays(ValidationError):
    pass
