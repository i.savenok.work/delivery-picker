from common.app_config import AppConfig


class DeliveryItemConfig(AppConfig):
    name = 'entities.delivery_item'
