from common.graphene import Node
from entities.delivery_item.models import DeliveryItem


class DeliveryItemType(Node):
    class Meta:
        model = DeliveryItem
        fields = ('id', 'name', 'not_available_on_wednesday')
