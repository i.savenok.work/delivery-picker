from common.graphene import List, ObjectType
from entities.delivery_item.models import DeliveryItem
from entities.delivery_item.schema.types import DeliveryItemType


class DeliveryItemQuery(ObjectType):
    delivery_items = List(DeliveryItemType)

    def resolve_delivery_items(self, info):
        return DeliveryItem.objects.order_by('created_at').all()
