from common.graphene import InputObjectType, List, Node, String
from entities.delivery.models import Delivery


class DeliveryType(Node):
    class Meta:
        model = Delivery
        fields = ('id', 'slot', 'items')

    def resolve_items(self: 'Delivery', info):
        return self.items.all()


class CreateDeliveryDataType(InputObjectType):
    slot_id = String(required=True)
    items_ids = List(String, required=True)
