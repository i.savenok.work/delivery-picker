from common.graphene import Field, Mutation, ObjectType
from entities.delivery.models import (
    Delivery,
    ItemCantBeDeliveredOnWednesday,
    SelectedSlotIsFirstInTheDayOfTheOddWeekOfTheYear,
    SelectedSlotIsFull
)
from entities.delivery.schema.types import CreateDeliveryDataType, DeliveryType
from entities.delivery_item.models import DeliveryItem
from entities.delivery_slot.models import DeliverySlot


class CreateDelivery(Mutation):
    class Arguments:
        data = CreateDeliveryDataType(required=True)
    delivery = Field(DeliveryType)

    ERR_SLOT_NOT_EXIST = '001'
    ERR_ITEMS_REQUIRED = '002'
    ERR_NOT_EXISTING_ITEMS = '003'
    ERR_SLOT_IS_FULL = '004'
    ERR_SLOT_IS_IN_FRIDAY_OF_THE_ODD_WEEK = '005'
    ERR_ITEM_NOT_AVAILABLE_ON_WEDNESDAYS = '006'

    @staticmethod
    def mutate(root, info, data: CreateDeliveryDataType):

        slot = DeliverySlot.objects.get_or_none(data.slot_id)
        if not slot:
            return CreateDelivery.with_errors([(
                CreateDelivery.ERR_SLOT_NOT_EXIST,
                f'Slot {data.slot_id} do not exist',
            )])

        if not data.items_ids:
            return CreateDelivery.with_errors([(CreateDelivery.ERR_ITEMS_REQUIRED, 'Items are required')])

        items = DeliveryItem.objects.filter(id__in=data.items_ids)
        not_existing_items = set(data.items_ids).difference([item.short_id for item in items])
        if not_existing_items:
            return CreateDelivery.with_errors([(
                CreateDelivery.ERR_NOT_EXISTING_ITEMS,
                f'Delivery items {",".join(not_existing_items)} does not exist',
            )])

        try:

            delivery = Delivery.objects.create(slot=slot, items=items)
            return CreateDelivery(delivery=delivery)

        except SelectedSlotIsFull:
            return CreateDelivery.with_errors([(
                CreateDelivery.ERR_SLOT_IS_FULL,
                f'Slot {data.slot_id} is out of capacity',
            )])
        except SelectedSlotIsFirstInTheDayOfTheOddWeekOfTheYear:
            return CreateDelivery.with_errors([(
                CreateDelivery.ERR_SLOT_IS_IN_FRIDAY_OF_THE_ODD_WEEK,
                f'Slot {data.slot_id} is first in Friday of the odd week of the year',
            )])
        except ItemCantBeDeliveredOnWednesday:
            return CreateDelivery.with_errors([(
                CreateDelivery.ERR_ITEM_NOT_AVAILABLE_ON_WEDNESDAYS,
                'One of selected items can\'t be delivered on Wednesday',
            )])


class DeliveryMutation(ObjectType):
    create_delivery = CreateDelivery.Field()
