from common.app_config import AppConfig


class DeliveryConfig(AppConfig):
    name = 'entities.delivery'
