from common import admin
from common.forms import ModelForm
from entities.delivery.models import Delivery


class DeliveryAdminForm(ModelForm):

    def clean(self):
        slot, items = self.cleaned_data.get('slot'), self.cleaned_data.get('items')
        if slot and items:
            self.instance.validate_items_for_slot(slot=slot, items=items)
        return super(DeliveryAdminForm, self).clean()


class DeliveryAdmin(admin.ModelAdmin):
    form = DeliveryAdminForm
    list_display = ('short_id',)
    readonly_fields = ('created_at', 'updated_at', 'deleted_at')
    fields = ('slot', 'items', 'created_at', 'updated_at', 'deleted_at')
    raw_id_fields = ('slot',)


admin.site.register(Delivery, DeliveryAdmin)
