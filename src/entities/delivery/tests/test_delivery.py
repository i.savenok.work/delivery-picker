import pytest

from common.datetime import add_date, date
from entities.delivery.models import (
    Delivery,
    ItemCantBeDeliveredOnWednesday,
    SelectedSlotIsFirstInTheDayOfTheOddWeekOfTheYear,
    SelectedSlotIsFull
)


@pytest.mark.django_db
class TestDelivery:

    def test_create(self, fake, delivery_slot_factory, delivery_item_factory):
        item, slot = delivery_item_factory(), delivery_slot_factory(max_capacity=1)
        delivery = Delivery.objects.create(slot=slot, items=[item])
        assert isinstance(delivery, Delivery)
        assert delivery.slot == slot
        slot.refresh_from_db()
        assert slot.get_deliveries_capacity() == 0
        assert list(delivery.items.all()) == [item]

    def test_update_items_set(self, delivery_factory, delivery_slot_factory, delivery_item_factory):
        delivery = delivery_factory()

        old_slot = delivery.slot
        old_slot_old_capacity = delivery.slot.get_deliveries_capacity()
        new_slot, item_to_add = delivery_slot_factory(), delivery_item_factory()
        new_slot_init_capacity = new_slot.get_deliveries_capacity()

        delivery.slot = new_slot
        delivery.items.add(item_to_add)
        delivery.save()

        assert old_slot_old_capacity == old_slot.get_deliveries_capacity() - 1
        assert new_slot_init_capacity == new_slot.get_deliveries_capacity() + 1
        assert item_to_add in delivery.items.all()

    def test_try_to_create_for_full_slot(self, delivery_slot_factory):
        with pytest.raises(SelectedSlotIsFull):
            Delivery.objects.create(slot=delivery_slot_factory(max_capacity=0), items=[])

    def test_try_to_change_slot_to_full_slot(self, delivery_factory, delivery_slot_factory):
        delivery = delivery_factory()
        full_slot = delivery_slot_factory(max_capacity=0)

        delivery.slot = full_slot
        with pytest.raises(SelectedSlotIsFull):
            delivery.save()

    @pytest.mark.django_db(transaction=True)
    def test_try_to_add_item_which_is_unavailable_on_wednesday(
        self,
        fake,
        delivery_slot_factory,
        delivery_factory,
        delivery_item_factory,
    ):
        wednesday = fake.datetime(iso_weekday=3).date()
        slot = delivery_slot_factory(date=wednesday)
        available_for_wednesday_item = delivery_item_factory(not_available_on_wednesday=False)
        delivery = delivery_factory(slot=slot, items=[available_for_wednesday_item])

        not_available_for_wednesday_item = delivery_item_factory(not_available_on_wednesday=True)
        with pytest.raises(ItemCantBeDeliveredOnWednesday):
            delivery.items.add(not_available_for_wednesday_item)

        not_available_for_wednesday_item.refresh_from_db()
        assert delivery not in not_available_for_wednesday_item.deliveries.all()

    def test_try_to_use_first_day_of_the_second_friday_of_the_week(self, delivery_slot_factory):
        odd_week_friday_date = date(year=2021, month=12, day=24)
        slot = delivery_slot_factory(date=odd_week_friday_date)
        with pytest.raises(SelectedSlotIsFirstInTheDayOfTheOddWeekOfTheYear):
            Delivery.objects.create(slot=slot, items=[])

    def test_get_future_wednesday_deliveries(self, fake, delivery_slot_factory, delivery_factory):
        wednesday_in_future_date = fake.datetime(year=+1, iso_weekday=3).date()
        future_wednesday_slot = delivery_slot_factory(date=wednesday_in_future_date)
        future_wednesday_delivery = delivery_factory(slot=future_wednesday_slot)

        future_thursday_slot = delivery_slot_factory(date=add_date(wednesday_in_future_date, days=1))
        delivery_factory(slot=future_thursday_slot)

        wednesday_in_past_date = fake.datetime(year=-1, iso_weekday=3).date()
        past_wednesday_slot = delivery_slot_factory(date=wednesday_in_past_date)
        delivery_factory(slot=past_wednesday_slot)

        future_wednesday_deliveries = Delivery.objects.get_future_wednesday_deliveries()
        assert future_wednesday_delivery in future_wednesday_deliveries
        assert len(future_wednesday_deliveries) == 1
