from typing import List

import pytest

from common.tests.utils import assert_api_error
from entities.delivery.models import Delivery
from entities.delivery.schema.mutations import CreateDelivery


def build_create_delivery_mutation(slot_id: str, items_ids: List[str]) -> str:
    return """
        mutation {
            createDelivery (
                data: {
                    slotId: \"""" + slot_id + """\",
                    itemsIds: [""" + ', '.join([f'\"{item_id}\"' for item_id in items_ids]) + """],
                }
            ) {
                delivery {
                    id
                    slot {
                        id
                        date
                        startTime
                        endTime
                        outOfCapacity
                        notAvailable
                    }
                    items {
                        id
                        name
                        notAvailableOnWednesday
                    }
                }
                errors {
                    code
                    desc
                }
            }
        }
    """


@pytest.mark.django_db
class TestCreateDeliveryMutation:

    def test_create_delivery(self, execute_graphql_query, delivery_item_factory, delivery_slot_factory):
        slot = delivery_slot_factory()
        item_1, item_2 = delivery_item_factory(), delivery_item_factory()

        response = execute_graphql_query(build_create_delivery_mutation(
            slot_id=slot.short_id,
            items_ids=[item_1.short_id, item_2.short_id],
        ))
        assert response['data']['createDelivery']['errors'] is None
        assert Delivery.objects.get_or_none(response['data']['createDelivery']['delivery']['id'])

    def test_try_to_create_delivery_without_items(self, execute_graphql_query, delivery_slot_factory):
        response = execute_graphql_query(build_create_delivery_mutation(
            slot_id=delivery_slot_factory().short_id,
            items_ids=[],
        ))
        assert_api_error(response['data']['createDelivery'], CreateDelivery.ERR_ITEMS_REQUIRED)

    def test_try_to_create_delivery_with_not_existing_slot(self, fake, execute_graphql_query, delivery_item_factory):
        response = execute_graphql_query(build_create_delivery_mutation(
            slot_id=fake.short_id(),
            items_ids=[delivery_item_factory().short_id],
        ))
        assert_api_error(response['data']['createDelivery'], CreateDelivery.ERR_SLOT_NOT_EXIST)

    def test_try_to_create_delivery_with_not_existing_item(
        self, fake, execute_graphql_query, delivery_item_factory, delivery_slot_factory
    ):
        slot = delivery_slot_factory()
        item_1_id, item_2_id = delivery_item_factory().short_id, fake.short_id()

        response = execute_graphql_query(build_create_delivery_mutation(
            slot_id=slot.short_id,
            items_ids=[item_1_id, item_2_id],
        ))
        assert_api_error(response['data']['createDelivery'], CreateDelivery.ERR_NOT_EXISTING_ITEMS)

    def test_try_to_create_delivery_for_full_slot(
        self, execute_graphql_query, delivery_item_factory, delivery_slot_factory, delivery_factory
    ):
        slot = delivery_slot_factory(max_capacity=1)
        delivery_factory(slot=slot)
        response = execute_graphql_query(build_create_delivery_mutation(
            slot_id=slot.short_id,
            items_ids=[delivery_item_factory().short_id],
        ))
        assert_api_error(response['data']['createDelivery'], CreateDelivery.ERR_SLOT_IS_FULL)

    def test_try_to_create_delivery_for_wednesday_with_not_deliverable_on_wednesday_item(
        self, fake, delivery_slot_factory, delivery_item_factory, execute_graphql_query
    ):
        slot = delivery_slot_factory(date=fake.datetime(iso_weekday=3))
        response = execute_graphql_query(build_create_delivery_mutation(
            slot_id=slot.short_id,
            items_ids=[delivery_item_factory(not_available_on_wednesday=True).short_id],
        ))
        assert_api_error(response['data']['createDelivery'], CreateDelivery.ERR_ITEM_NOT_AVAILABLE_ON_WEDNESDAYS)

    def test_try_to_create_delivery_for_odd_week_friday(
        self, execute_graphql_query, delivery_item_factory, delivery_slot_factory, fake
    ):
        odd_week_friday = fake.datetime(year=2021, month=12, day=24).date()
        slot = delivery_slot_factory(date=odd_week_friday)
        response = execute_graphql_query(build_create_delivery_mutation(
            slot_id=slot.short_id,
            items_ids=[delivery_item_factory().short_id],
        ))
        assert_api_error(response['data']['createDelivery'], CreateDelivery.ERR_SLOT_IS_IN_FRIDAY_OF_THE_ODD_WEEK)
