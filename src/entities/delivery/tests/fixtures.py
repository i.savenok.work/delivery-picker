from typing import List

import pytest

from entities.delivery.models import Delivery
from entities.delivery_item.models import DeliveryItem
from entities.delivery_slot.models import DeliverySlot


@pytest.fixture
@pytest.mark.django_db
def delivery_factory(fake, delivery_slot_factory, delivery_item_factory):
    def create(slot: 'DeliverySlot' = None, items: List['DeliveryItem'] = None) -> 'Delivery':
        delivery = Delivery.objects.create(
            slot=slot or delivery_slot_factory(),
            items=items or [delivery_item_factory()],
        )
        return delivery
    return create
