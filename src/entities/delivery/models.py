from typing import TYPE_CHECKING, Iterable, Optional

from common import models
from common.datetime import date, is_wednesday
from common.exceptions import ValidationError
from common.models import Q

if TYPE_CHECKING:
    from entities.delivery_item.models import DeliveryItem, DeliveryItemManager
    from entities.delivery_slot.models import DeliverySlot


class DeliveryManager(models.Manager):

    def create(
        self,
        slot: 'DeliverySlot',
        items: Iterable['DeliveryItem'],
    ) -> 'Delivery':
        delivery = super(DeliveryManager, self).create(slot=slot)
        delivery.items.set(items)
        return delivery

    def get_future_wednesday_deliveries(self):
        return self.filter(
            Q(slot__date__gte=date.today())
            &
            Q(slot__date__week_day=4)
        ).all()


class Delivery(models.Model):
    slot: 'DeliverySlot' = models.ForeignKey(
        'delivery_slot.DeliverySlot',
        on_delete=models.CASCADE,
        null=False,
        related_name='deliveries',
    )
    items: 'DeliveryItemManager' = models.ManyToManyField('delivery_item.DeliveryItem', related_name='deliveries')

    objects = DeliveryManager()

    __current_slot: 'DeliverySlot' = None

    def __init__(self, *args, **kwargs):
        super(Delivery, self).__init__(*args, **kwargs)
        self.__current_slot = getattr(self, 'slot', None)

    @classmethod
    def validate_items_for_slot(cls, slot: 'DeliverySlot', items: Iterable['DeliveryItem']):
        if is_wednesday(slot.date):
            for item in items:
                if item.not_available_on_wednesday:
                    raise ItemCantBeDeliveredOnWednesday(f'{item} can\'t be delivered on wednesday')

    def validate(self):
        is_creation = self.created_at is None
        new_slot: Optional['DeliverySlot'] = getattr(self, 'slot', None)
        is_slot_changed = new_slot != self.__current_slot
        if is_slot_changed or (is_creation and new_slot):
            is_slot_available = new_slot.get_deliveries_capacity() > 0
            if not is_slot_available:
                raise SelectedSlotIsFull(f'Selected slot {new_slot} is not available for new deliveries')

            if new_slot.is_first_for_friday_of_the_odd_week_of_the_year():
                raise SelectedSlotIsFirstInTheDayOfTheOddWeekOfTheYear('Selected slot is not available')

    def __str__(self) -> str:
        return self.short_id

    class Meta:
        verbose_name_plural = 'Deliveries'


class SelectedSlotIsFull(ValidationError):
    pass


class SelectedSlotIsFirstInTheDayOfTheOddWeekOfTheYear(ValidationError):
    pass


class ItemCantBeDeliveredOnWednesday(ValidationError):
    pass
