from common.signals import m2m_changed, receiver
from entities.delivery.models import Delivery


@receiver(m2m_changed, sender=Delivery.items.through)
def validate_delivery_items_change(sender, instance: Delivery, action, reverse, model, pk_set, **kwargs):
    Delivery.validate_items_for_slot(slot=instance.slot, items=instance.items.all())
