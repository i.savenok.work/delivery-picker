from django.core.exceptions import ValidationError as _ValidationError
from django.core.management import CommandError as _CommandError
from django.db import DataError as _DataError
from django.db import IntegrityError as _IntegrityError

IntegrityError = _IntegrityError
DataError = _DataError
ValidationError = _ValidationError
CommandError = _CommandError
