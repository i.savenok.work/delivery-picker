from typing import Iterable, Tuple

from graphene.types import Boolean as _Boolean
from graphene.types import Date as _Date
from graphene.types import Field as _Field
from graphene.types import InputObjectType as _InputObjectType
from graphene.types import Int as _Int
from graphene.types import JSONString as _JSONString
from graphene.types import List as _List
from graphene.types import Mutation as _Mutation
from graphene.types import ObjectType as _ObjectType
from graphene.types import Schema as _Schema
from graphene.types import String as _String
from graphene_django import DjangoListField as _DjangoListField
from graphene_django import DjangoObjectType as _DjangoObjectType

Schema = _Schema
ObjectType = _ObjectType
Field = _Field
List = _List
String = _String
JSONString = _JSONString
DjangoListField = _DjangoListField
Date = _Date
Int = _Int
Boolean = _Boolean
InputObjectType = _InputObjectType


class Node(_DjangoObjectType):
    class Meta:
        abstract = True

    id = _String()

    def resolve_id(self, info) -> str: return self.id.hex

    @classmethod
    def get_queryset(cls, queryset, info):
        return queryset


class ApiError(_ObjectType):
    desc = _String()
    code = _String()


class Mutation(_Mutation):
    class Meta:
        abstract = True

    errors = _List(ApiError)

    @classmethod
    def with_errors(cls, errors: Iterable[Tuple[str, str]]):
        return cls(errors=[ApiError(code=err[0], desc=err[1]) for err in errors])
