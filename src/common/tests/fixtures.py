import uuid
from typing import Callable

import pytest
from django.contrib.auth.models import AnonymousUser
from django.test.client import RequestFactory
from faker import Faker
from faker.providers import BaseProvider
from graphene.test import Client

from common.datetime import date, datetime, timedelta, tz
from project.schema import schema as api_schema
from project.settings import API_URL_PREFIX

faker = Faker()


class Provider(BaseProvider):
    unique: 'Provider'
    word: Callable
    pyint: Callable

    @staticmethod
    def datetime(
        year: int = None,  # year; +1 - next year; 0 - current year; -1 - previous year
        month: int = None,
        day: int = None,
        hour: int = None,
        iso_weekday: int = None,
    ) -> datetime:
        year = {
            -1: date.today().year - 1,
            0: date.today().year,
            +1: date.today().year + 1,
        }.get(year)

        result_datetime = datetime(
            year=year if year is not None else 2021,
            month=month if month is not None else 12,
            day=day if day is not None else 23,
            hour=hour if hour is not None else 10,
            tzinfo=tz.utc,
        )

        if iso_weekday:
            if day:
                raise ValueError('"day" and "iso_weekday" should not be provided at the same time')
            closest_weekday = result_datetime + timedelta((iso_weekday - result_datetime.isoweekday()) % 7)
            return closest_weekday

        return result_datetime

    @staticmethod
    def short_id():
        return uuid.uuid4().hex


faker.add_provider(Provider)


@pytest.fixture(scope='session')
def fake() -> Provider:
    return faker  # noqa


test_client = Client(api_schema)


@pytest.fixture()
def execute_graphql_query():
    def request(query: str, **kwargs) -> dict:
        context_value = RequestFactory().post(API_URL_PREFIX)
        context_value.user = AnonymousUser()

        return test_client.execute(
            query,
            context_value=context_value,
            **kwargs,
        )

    return request
