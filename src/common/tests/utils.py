

def assert_api_error(response: dict, code: str):
    assert code in [err['code'] for err in response['errors']]
