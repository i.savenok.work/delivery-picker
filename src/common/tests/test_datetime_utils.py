from common.datetime import (
    datetime,
    is_friday,
    is_odd_week_in_year,
    is_wednesday
)


def test_define_is_wednesday(fake):
    tuesday_date = fake.datetime(iso_weekday=2)
    wednesday_date = fake.datetime(iso_weekday=3)
    assert not is_wednesday(tuesday_date)
    assert is_wednesday(wednesday_date)


def test_define_is_friday(fake):
    tuesday_date = fake.datetime(iso_weekday=2)
    friday_date = fake.datetime(iso_weekday=5)
    assert not is_friday(tuesday_date)
    assert is_friday(friday_date)


def test_define_is_odd_week_in_year():
    odd_week_datetime = datetime(year=2021, month=12, day=21)
    even_week_datetime = datetime(year=2021, month=12, day=19)
    assert is_odd_week_in_year(odd_week_datetime)
    assert not is_odd_week_in_year(even_week_datetime)
