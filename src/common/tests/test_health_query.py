import pytest


@pytest.mark.django_db
def test_health_query(execute_graphql_query):
    assert execute_graphql_query('query {health}') == {'data': {'health': '{"status": "ok"}'}}
