from django.db import connection

from common import graphene


class HealthQuery(graphene.ObjectType):
    health = graphene.JSONString()

    @staticmethod
    def resolve_health(self, info):
        try:
            connection.ensure_connection()
            return {'status': 'ok'}
        except Exception as e:
            return {'status': 'error', 'detail': str(e)}
