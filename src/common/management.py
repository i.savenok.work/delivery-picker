from django.core.management import BaseCommand as _BaseCommand
from django.core.management import CommandParser as _CommandParser
from django.core.management import call_command as _call_command


class BaseCommand(_BaseCommand):
    def handle(self, *args, **options):
        pass


call_command = _call_command
CommandParser = _CommandParser
