from typing import Optional
from uuid import UUID, uuid4

from django.db import models as _models

CASCADE = _models.CASCADE


class Q(_models.Q):
    pass


class UUIDField(_models.UUIDField):
    pass


class DateTimeField(_models.DateTimeField):
    pass


class CharField(_models.CharField):
    pass


class BooleanField(_models.BooleanField):
    pass


class TimeField(_models.TimeField):
    pass


class DateField(_models.DateField):
    pass


class PositiveSmallIntegerField(_models.PositiveSmallIntegerField):
    pass


class ManyToManyField(_models.ManyToManyField):
    pass


class ForeignKey(_models.ForeignKey):
    pass


class QuerySet(_models.QuerySet):
    pass


class Manager(_models.manager.BaseManager.from_queryset(QuerySet)):

    def get_or_none(self, obj_id) -> Optional['Model']:
        return self.filter(id=obj_id).first()


class Model(_models.Model):
    id: UUID = UUIDField(primary_key=True, db_index=True, default=uuid4, editable=False)

    created_at = DateTimeField(auto_now_add=True)
    updated_at = DateTimeField(auto_now=True)
    deleted_at = DateTimeField(null=True, blank=True)

    @property
    def short_id(self) -> str:
        return self.id.hex

    is_validated = False

    def validate(self):
        pass

    def clean(self):
        self.validate()
        self.is_validated = True

    def save(
        self,
        force_insert=False,
        force_update=False,
        using=None,
        update_fields=None,
    ):
        if not self.is_validated:
            self.validate()

        super().save(force_insert, force_update, using, update_fields)

    def before_delete(self):
        pass

    def delete(
        self,
        using=None,
        keep_parents=False
    ):
        self.before_delete()
        super().delete(using=using, keep_parents=keep_parents)

    class Meta:
        abstract = True


Count = _models.Count
F = _models.F
