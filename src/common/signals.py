from django.db.models.signals import m2m_changed as _m2m_changed
from django.dispatch import receiver as _receiver

m2m_changed = _m2m_changed
receiver = _receiver
