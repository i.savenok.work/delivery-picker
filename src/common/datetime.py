from datetime import date as _date
from datetime import datetime as _datetime
from datetime import time as _time
from datetime import timedelta as _timedelta
from datetime import timezone as _timezone
from typing import Union

datetime = _datetime
time = _time
date = _date
timedelta = _timedelta
tz = _timezone


def is_wednesday(point_in_time: Union[datetime, date]) -> bool:
    return point_in_time.isoweekday() == 3


def is_friday(point_in_time: Union[datetime, date]) -> bool:
    return point_in_time.isoweekday() == 5


def is_odd_week_in_year(point_in_time: Union[datetime, date]) -> bool:
    return bool(point_in_time.isocalendar().week % 2)  # type: ignore


def add_time(time_obj: time, hours: int) -> time:
    datetime_obj = datetime.combine(date.today(), time_obj)
    return (datetime_obj + timedelta(hours=hours)).time()


def add_date(date_obj: date, days: int) -> date:
    datetime_obj = datetime.combine(date_obj, time())
    return (datetime_obj + timedelta(days=days)).date()
